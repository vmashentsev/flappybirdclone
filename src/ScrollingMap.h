#ifndef _SCROLLINGMAP_H_
#define _SCROLLINGMAP_H_

#include "Component.h"
#include "PipesPair.h"
#include "GameState.h"
#include "AnimatedInterlacedSprite.h"
#include <list>

namespace FlappyBirdClone {
	class ScrollingMap : public Component {
	public:
		ScrollingMap(GameObject* parent, AnimatedInterlacedSprite* ground,
			PlayGameState::Substate& substate, int width, int leftBorder = 0);

		virtual void Update() override;

		void StopScrolling() { scrollSpeed = 0; }

	private:
		void AddPipe(int x);
		void RemoveFirstPipe();

		/// pipes and triggers (elements) are the components of some gameobjects
		/// they will be deleted when gameobjects will be deleted, and no
		/// need to delete pipes (triggers) content in the ScrollingMap destructor
		std::list<PipesPair*> pipes;
		std::list<TriggerCollision*> triggers;
		AnimatedInterlacedSprite* ground;
		PlayGameState::Substate& playGameState;
		int scrollSpeed  = -100; //px per sec
		int pipeInterval = 100;  //px between pipes
		int leftBorder;
		int rightBorder;
		int heightRegion;
	};
}
#endif // !_SCROLLINGMAP_H_