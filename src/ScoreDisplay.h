#ifndef _SCOREDISPLAY_H_
#define _SCOREDISPLAY_H_

#include "Component.h"
#include <functional>

namespace FlappyBirdClone {
	class ScoreDisplay: public Component {
	public:
		ScoreDisplay(GameObject* parent, const std::string& id,
			int x, int y, int w, int h, int margin, std::function<int(void)> func)
			: Component(parent), textureID{ id }, sx{ x }, sy{ y },
			width{ w }, height{ h }, margin{ margin }, scoreGetter{ func }
		{}

		virtual void Draw() override;
		virtual void Update() override;

	private:
		std::string textureID;
		int sx;
		int sy;
		int width;
		int height;
		int margin;
		int score = 0;
		std::function<int(void)> scoreGetter;
	};
}
#endif // !_SCOREDISPLAY_H_