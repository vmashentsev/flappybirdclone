#ifndef _CLOCK_H_
#define _CLOCK_H_

#include <chrono>

namespace FlappyBirdClone {
	class Clock {
	public:
		virtual float GetDelta() = 0;
		virtual float GetTime()  = 0;
		virtual void Start() = 0;
		virtual void Stop()  = 0;
	};

	class ClockChrono : public Clock {
	public:
		ClockChrono(): point{ clock.now() }, start{ point } {}

		virtual float GetDelta() override;
		virtual float GetTime() override;
		virtual void Start() override;
		virtual void Stop() override;

	private:
		float deltaTime = 0.0f;
		std::chrono::steady_clock clock;
		std::chrono::time_point<std::chrono::steady_clock> point;
		std::chrono::time_point<std::chrono::steady_clock> start;
	};
}

#endif // !_CLOCK_H_