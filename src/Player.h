#ifndef _PLAYER_H_
#define	_PLAYER_H_

#include "Component.h"
#include "Collidable.h"
#include "GameState.h"

namespace FlappyBirdClone {
	class Player: public Component {
	public:
		Player(GameObject* parent, const std::string& id,
			const PlayGameState::Substate& substate);
	
		virtual void Draw() override;
		virtual void Update() override;

		int GetHeight() const { return height; }
		int GetWidth()  const { return width;  }

	private:
		std::string spriteID;
		const PlayGameState::Substate& playGameState;
		int framePerSec = 8;
		int frameCount = 4;
		int frame   = 0;
		int width   = 34;
		int height  = 34;
		int sx      = 6;
		int sy      = 977;
		int margin  = 22;
		float gravity = 500.0f;
		float vspeed  = 10.0f;
		float mvspeed = 1000.0f;
		float climbSpeed = -100.0f;

		float pitch = 0.0f;
		float maxPitch = 90.0f;
		float pitchSpeed = 60.0f;
		float pitchMaxSpeed = 180.0f;
		float pitchAccel = 60.0f;
		float pitchUpSpeed = -45.0f;
	};
}
#endif	// !_PLAYER_H_