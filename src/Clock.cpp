#include "Clock.h"

typedef std::chrono::duration<float> sec;

float FlappyBirdClone::ClockChrono::GetDelta()
{
	return deltaTime;
}

float FlappyBirdClone::ClockChrono::GetTime()
{
	return  std::chrono::duration_cast<sec>(point - start).count();
}

void FlappyBirdClone::ClockChrono::Start()
{
	point = clock.now();
}

void FlappyBirdClone::ClockChrono::Stop()
{
	auto now = clock.now();
	auto dt = now - point;
	point = now;
	deltaTime = std::chrono::duration_cast<sec>(dt).count();
}