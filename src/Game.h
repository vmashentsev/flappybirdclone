#ifndef _GAME_H_
#define	_GAME_H_

#include "TextureManager.h"
#include "InputHandler.h"
#include "GameStateHandler.h"
#include "CollisionManager.h"
#include "ScoreHandler.h"
#include "Clock.h"
#include "SoundManager.h"
#include <SDL.h>

namespace FlappyBirdClone {
	class Game {
	public:
		void Clean();
		bool Init();
		void HandleEvents();
		void Render();
		void Update();
	
		inline bool IsRunning() { return isRunning; }

		static void Run();

		static SDL_Window*       GetWindow()           { return instance.window;           }
		static SDL_Renderer*     GetRenderer()         { return instance.renderer;         }
		static GameStateHandler* GetStateHandler()     { return instance.gameStateHandler; }
		static InputHandler*     GetInputHandler()     { return instance.inputHandler;     }
		static TextureManager*   GetTextureManager()   { return instance.textureManager;   }
		static CollisionManager* GetCollisionManager() { return instance.collisionManager; }
		static ScoreHandler*     GetScoreHandler()     { return instance.scoreHandler;     }
		static SoundManager*     GetSoundManager()     { return instance.soundManager;     }
		static Clock*            GetClock()            { return instance.gameClock;        }
		static void              Quit()                { instance.isRunning = false;       }
	
	private:
		Game() { }
		SDL_Window*       window;
		SDL_Renderer*     renderer;
		GameStateHandler* gameStateHandler;
		InputHandler*     inputHandler;
		TextureManager*   textureManager;
		CollisionManager* collisionManager;
		ScoreHandler*     scoreHandler;
		SoundManager*     soundManager;
		Clock*            gameClock;
		bool isRunning;

		static Game instance;
	};
}
#endif // !_GAME_H_