#ifndef _COLLISIONMANAGER_H_
#define _COLLISIONMANAGER_H_

#include <vector>

namespace FlappyBirdClone {
	class Collidable;

	class CollisionManager {
	public:
		bool CheckCollision(Collidable* active);
	
		template<typename CollidableType>
		bool CheckCollision(Collidable* active);

		void AddCollidable(Collidable* object);
		void RemoveCollidable(Collidable* object);
	private:
		bool Collide(Collidable* active, Collidable* other);
		std::vector<Collidable*> objects;
	};

	template<typename CollidableType>
	bool CollisionManager::CheckCollision(Collidable* active)
	{
		for (auto& i : objects) {
			if (dynamic_cast<CollidableType*>(i) == nullptr || i == active) {
				continue;
			}
			if (Collide(active, i)) {
				return true;
			}
		}
		return false;
	}
}

#endif // !_COLLISIONMANAGER_H_