#ifndef _STATICSPRITE_H_
#define _STATICSPRITE_H_

#include "Component.h"

namespace FlappyBirdClone {
	class StaticSprite : public Component {
	public:
		StaticSprite(GameObject* parent, const std::string& id, int x, int y, int w, int h)
			: Component(parent), spriteID{ id }, sx{ x }, sy{ y }, width{ w }, height{ h } { }
		virtual void Draw() override;
		virtual void Update() override;

	private:
		std::string spriteID;
		int width;
		int height;
		int sx;
		int sy;
	};
}
#endif // !_STATICSPRITE_H_