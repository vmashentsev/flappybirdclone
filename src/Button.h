#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "Component.h"
#include <string>
#include <functional>

namespace FlappyBirdClone {
	class Button: public Component {
	public:
		Button(GameObject* parent, const std::string& spriteID,
			int x, int y, int w, int h, const std::function<void()>& handler)
			: Component(parent), spriteID{ spriteID }, x{ x }, y{ y }, width{ w },
			height{ h }, handler{ handler }	{}
		virtual void Draw() override;
		virtual void Update() override;

	private:
		bool IsMouseInside();

		std::string spriteID;
		int x;
		int y;
		int width;
		int height;
		std::function<void()> handler; ///click handler

		bool pressed = false;
	};
}

#endif // !_BUTTON_H_