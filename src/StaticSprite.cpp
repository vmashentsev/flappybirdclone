#include "StaticSprite.h"
#include "Game.h"
#include "Transform.h"

void FlappyBirdClone::StaticSprite::Draw()
{
	auto transform = GetParent()->GetTransform();
	Game::GetTextureManager()->Render(
		Game::GetRenderer(), spriteID,
		sx, sy,
		width, height,
		transform->X, transform->Y);
}

void FlappyBirdClone::StaticSprite::Update()
{
	//just empty
}
