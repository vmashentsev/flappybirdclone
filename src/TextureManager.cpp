#include "TextureManager.h"
#include <SDL_image.h>

FlappyBirdClone::TextureManager::~TextureManager()
{
	for (auto& i : textureMap) {
		SDL_DestroyTexture(i.second);
	}
}

bool FlappyBirdClone::TextureManager::Load(const std::string & fileName, const std::string & id, SDL_Renderer * renderer)
{
	SDL_Surface* tempSurface = IMG_Load(fileName.c_str());
	if (tempSurface == nullptr)
	{
		return false;
	}
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, tempSurface);
	SDL_FreeSurface(tempSurface);
	if (texture == nullptr)
	{
		return false;
	}
	textureMap[id] = texture;
	return true;
}

void FlappyBirdClone::TextureManager::Render(
	SDL_Renderer * renderer,
	const std::string& id,
	int sx, int sy, int w, int h,
	float dx, float dy)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;
	srcRect.x = sx;
	srcRect.y = sy;
	srcRect.w = destRect.w = w;
	srcRect.h = destRect.h = h;
	destRect.x = (int)dx;
	destRect.y = (int)dy;
	SDL_RenderCopy(renderer, textureMap[id], &srcRect, &destRect);
}

void FlappyBirdClone::TextureManager::RenderFrame(
	SDL_Renderer * renderer,
	const std::string& id,
	int sx, int sy, int w, int h,
	float dx, float dy, int frame,
	int margin, bool row)
{
	RenderFrame(renderer, id, sx, sy, w, h, dx, dy, 0, frame, margin, row);
}

void FlappyBirdClone::TextureManager::RenderFrame(
	SDL_Renderer * renderer,
	const std::string& id,
	int sx, int sy, int w, int h,
	float dx, float dy, float angle, int frame,
	int margin, bool row)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;
	srcRect.x = sx + (row ? (w + margin) * frame : 0);
	srcRect.y = sy + (!row ? (h + margin) * frame : 0);
	srcRect.w = destRect.w = w;
	srcRect.h = destRect.h = h;
	destRect.x = (int)dx;
	destRect.y = (int)dy;
	SDL_RenderCopyEx(renderer, textureMap[id], &srcRect, &destRect, angle, nullptr, SDL_FLIP_NONE);
}

void FlappyBirdClone::TextureManager::RenderFrameInterlaced(
	SDL_Renderer * renderer,
	const std::string& id,
	int sx, int sy, int w, int h,
	float dx, float dy, int offset,
	int frame, bool row)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;
	srcRect.x = sx + (row  ? offset * frame : 0);
	srcRect.y = sy + (!row ? offset * frame : 0);
	srcRect.w = destRect.w = w;
	srcRect.h = destRect.h = h;
	destRect.x = (int)dx;
	destRect.y = (int)dy;
	SDL_RenderCopy(renderer, textureMap[id], &srcRect, &destRect);
}