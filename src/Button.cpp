#include "Button.h"
#include "Game.h"
#include "Transform.h"
#include "TextureManager.h"

void FlappyBirdClone::Button::Draw()
{
	auto transform = GetParent()->GetTransform();
	Game::GetTextureManager()->Render(Game::GetRenderer(), spriteID,
		x, y, width, height, transform->X, transform->Y);
}

void FlappyBirdClone::Button::Update()
{
	auto input = Game::GetInputHandler();
	if (input->GetLeftMouseDown()) {
		if (IsMouseInside()) {
			pressed = true;
		}
		return;
	}
	if (pressed && input->GetLeftMouseUp() && IsMouseInside()) {
		pressed = false;
		handler();
	}
}

bool FlappyBirdClone::Button::IsMouseInside()
{
	float left = GetParent()->GetTransform()->X;
	float top = GetParent()->GetTransform()->Y;
	auto mpos = Game::GetInputHandler()->GetMousePosition();
	int mposx = std::get<0>(mpos);
	int mposy = std::get<1>(mpos);
	if (left <= mposx && mposx <= left + width &&
		top <= mposy && mposy <= top + height) {

		return true;
	}
	return false;
}