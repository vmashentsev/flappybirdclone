#include "GameObject.h"
#include "Game.h"
#include "Transform.h"
#include <algorithm>

FlappyBirdClone::GameObject::GameObject()
{
	components.push_back(new Transform(this, 0, 0));
}

FlappyBirdClone::GameObject::GameObject(float x, float y)
{
	components.push_back(new Transform(this, x, y));
}

FlappyBirdClone::GameObject::~GameObject()
{
	for (auto& object : objects) {
		delete object;
	}
	for (auto& component : components) {
		delete component;
	}
}

void FlappyBirdClone::GameObject::Draw()
{
	for (auto& c : components) {
		c->Draw();
	}
	for (auto& object : objects) {
		object->Draw();
	}
}

void FlappyBirdClone::GameObject::Update()
{
	for (auto& c : components) {
		c->Update();
		if (!Game::GetStateHandler()->IsValid()) {
			return;
		}
	}
	for (auto& object : objects) {
		object->Update();
		if (!Game::GetStateHandler()->IsValid()) {
			return;
		}
	}
}

void FlappyBirdClone::GameObject::AddComponent(Component* component)
{
	components.push_back(component);
}

void FlappyBirdClone::GameObject::AddChild(GameObject* object)
{
	objects.push_back(object);
}

void FlappyBirdClone::GameObject::RemoveChild(GameObject* object)
{
	auto iter = std::find(objects.begin(), objects.end(), object);
	if (iter == objects.end()) {
		return;
	}
	delete object;
	objects.erase(iter);
}

FlappyBirdClone::Transform* FlappyBirdClone::GameObject::GetTransform() const
{
	return static_cast<Transform*>(components[0]);
}