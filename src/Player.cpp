#include "Player.h"
#include "Game.h"
#include "Transform.h"
#include "TextureManager.h"
#include "Collidable.h"

#include <iostream>

FlappyBirdClone::Player::Player(GameObject* parent, const std::string& id,
	const PlayGameState::Substate& substate)
	: Component(parent), spriteID{ id }, playGameState{ substate }
{
	//empty
}

void FlappyBirdClone::Player::Draw()
{
	auto transform = GetParent()->GetTransform();
	Game::GetTextureManager()->RenderFrame(Game::GetRenderer(), spriteID,
		sx, sy, width, height, transform->X, transform->Y, pitch, frame, margin);
}

void FlappyBirdClone::Player::Update()
{
	float dt = Game::GetClock()->GetDelta();
	frame = int(Game::GetClock()->GetTime() * framePerSec) % frameCount;
	frame = frame >= 3 ? 1 : frame;
	auto transform = GetParent()->GetTransform();
	vspeed += gravity * dt;
	if (vspeed >= mvspeed) {
		vspeed = mvspeed;
	}
	transform->Y += vspeed * dt;
	int w = 0, h = 0;
	SDL_GetWindowSize(Game::GetWindow(), &w, &h);
	if (transform->Y >= h) {
		transform->Y = (float)h - 23;
	}
	if (transform->Y < 0) {
		transform->Y = 0;
	}
	pitchSpeed += pitchAccel * dt;
	if (pitchSpeed >= pitchMaxSpeed) {
		pitchSpeed = pitchMaxSpeed;
	}
	pitch += pitchSpeed * dt;
	if (pitch >= maxPitch) {
		pitch = maxPitch;
	}
	if (playGameState == PlayGameState::Substate::NORMAL) {
		if (Game::GetInputHandler()->GetLeftMouseDown()) {
			Game::GetSoundManager()->Play("wing");
		}
		if (Game::GetInputHandler()->GetLeftMousePress()) {
			vspeed = climbSpeed;
			pitch = pitchUpSpeed;
		}
	}
	auto playerCol = dynamic_cast<Collidable*>(this->GetParent()->GetComponent<Collidable>());
	if (playerCol != nullptr) {
		if (playGameState == PlayGameState::Substate::HITTED) {
			Game::GetCollisionManager()->CheckCollision<EndGameCollision>(playerCol);
		} else {
			Game::GetCollisionManager()->CheckCollision(playerCol);
		}
	}
}