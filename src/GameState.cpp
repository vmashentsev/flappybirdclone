#include "Game.h"
#include "GameState.h"
#include "GameObject.h"
#include "Transform.h"
#include "StaticSprite.h"
#include "AnimatedInterlacedSprite.h"
#include "ScrollingMap.h"
#include "Player.h"
#include "ScoreDisplay.h"
#include "Button.h"
#include <iostream>

void FlappyBirdClone::MainMenuState::Render()
{
	mainScene->Draw();
}

void FlappyBirdClone::MainMenuState::Update()
{
	if (Game::GetInputHandler()->GetLeftMouseDown()) {
		Game::GetStateHandler()->Add(new PlayGameState);
		return;
	}
	mainScene->Update();
}

bool FlappyBirdClone::MainMenuState::OnEnter()
{
	std::cout << "MainMenu state enter" << std::endl;
	///Load all neccessary texture
	std::string textureID = "main_atlas";
	Game::GetTextureManager()->Load("Assets\\Atlas.png", textureID, Game::GetRenderer());
	mainScene = new GameObject;
	///background
	int bkgHeight = 512;
	int bkgWidth  = 288;
	int bkgX = 0;
	int bkgY = 0;
	auto background = new GameObject;
	background->AddComponent(new StaticSprite(background, textureID,
		bkgX, bkgY, bkgWidth, bkgHeight));
	mainScene->AddChild(background);
	///ground
	int groundHeight = 112;
	int groundWidth = 288;
	int groundX = 584;
	int groundY = 0;
	int groundSpriteOffset = 2;
	auto ground = new GameObject;
	int winW = 0, winH = 0;
	SDL_GetWindowSize(Game::GetWindow(), &winW, &winH);
	ground->GetTransform()->Y = (float)winH - groundHeight;
	auto groundComp = new AnimatedInterlacedSprite(ground, textureID, groundX, groundY,
		groundWidth, groundHeight, groundSpriteOffset);
	ground->AddComponent(groundComp);
	mainScene->AddChild(ground);
	///title
	int titleHeight = 48;
	int titleWidth = 178;
	int titleX = 702;
	int titleY = 182;
	auto title = new GameObject;
	title->GetTransform()->X = 55;
	title->GetTransform()->Y = 232;
	auto titleComp = new StaticSprite(title, textureID, titleX, titleY, titleWidth, titleHeight);
	title->AddComponent(titleComp);
	mainScene->AddChild(title);
	///helpLogo
	int helpHeight = 98;
	int helpWidth = 114;
	int helpX = 584;
	int helpY = 182;
	auto helpObject = new GameObject;
	helpObject->GetTransform()->X = (winW - helpWidth) * 0.5f;
	helpObject->GetTransform()->Y = 284;
	auto help = new StaticSprite(helpObject, textureID, helpX, helpY, helpWidth, helpHeight);
	helpObject->AddComponent(help);
	mainScene->AddChild(helpObject);
	return true;
}

bool FlappyBirdClone::MainMenuState::OnExit()
{
	std::cout << "MainMenu state exit" << std::endl;
	delete mainScene;
	return true;
}

void FlappyBirdClone::PlayGameState::Render()
{
	mainScene->Draw();
}

void FlappyBirdClone::PlayGameState::Update()
{
	mainScene->Update();
}

bool FlappyBirdClone::PlayGameState::OnEnter()
{
	int winW = 0, winH = 0;
	SDL_GetWindowSize(Game::GetWindow(), &winW, &winH);
	std::string textureID = "main_atlas";
	mainScene = new GameObject;
	///background
	int bkgHeight = 512;
	int bkgWidth = 288;
	int bkgX = 0;
	int bkgY = 0;
	auto background = new GameObject;
	background->AddComponent(new StaticSprite(background, textureID,
		bkgX, bkgY, bkgWidth, bkgHeight));
	mainScene->AddChild(background);
	///ground forward decl
	int groundHeight = 112;
	int groundWidth = 288;
	int groundX = 584;
	int groundY = 0;
	int groundSpriteOffset = 2;
	auto groundObject = new GameObject;
	groundObject->GetTransform()->Y = (float)winH - groundHeight;
	auto ground = new AnimatedInterlacedSprite(groundObject, textureID,
		groundX, groundY, groundWidth, groundHeight, groundSpriteOffset);
	groundObject->AddComponent(ground);
	auto groundCollider = new AABBCollider(0, 0, (float)groundWidth, (float)groundHeight);
	groundObject->AddComponent(new EndGameCollision(groundObject, groundCollider));
	//map with pipes
	auto scrollmapObject = new GameObject;
	auto scrollmap = new ScrollingMap(scrollmapObject, ground, currentState, winW);
	scrollmapObject->AddComponent(scrollmap);
	mainScene->AddChild(scrollmapObject);
	///player
	auto playerObject = new GameObject;
	playerObject->GetTransform()->X = 80;
	playerObject->GetTransform()->Y = 200;
	auto player = new Player(playerObject, textureID, currentState);
	playerObject->AddComponent(player);
	auto playerCollider = new AABBCollider(0, 0, (float)player->GetWidth(), (float)player->GetHeight());
	playerObject->AddComponent(new EmptyCollision(playerObject, playerCollider));
	mainScene->AddChild(playerObject);
	///ground finale decl
	mainScene->AddChild(groundObject);
	///scores
	int scoreX = 584;
	int scoreY = 320;
	int scoreW = 24;
	int scoreH = 36;
	int scoreMargin = 4;
	auto scoreGetter = []()-> int { return Game::GetScoreHandler()->GetLastScore(); };
	auto scoresObject = new GameObject;
	scoresObject->GetTransform()->X = winW / 2.0f;
	scoresObject->GetTransform()->Y = winH / 8.0f;
	auto scores = new ScoreDisplay(scoresObject, textureID, scoreX, scoreY,
		scoreW, scoreH, scoreMargin, scoreGetter);
	scoresObject->AddComponent(scores);
	mainScene->AddChild(scoresObject);
	return true;
}

bool FlappyBirdClone::PlayGameState::OnExit()
{
	std::cout << "Play state exit" << std::endl;
	delete mainScene;
	return true;
}

void FlappyBirdClone::GameOverState::Render()
{
	mainScene->Draw();
}

void FlappyBirdClone::GameOverState::Update()
{
	mainScene->Update();
}

bool FlappyBirdClone::GameOverState::OnExit()
{
	std::cout << "Gameover state exit" << std::endl;
	Game::GetScoreHandler()->ResetLastScore();
	delete mainScene;
	return true;
}

bool FlappyBirdClone::GameOverState::OnEnter()
{
	/// Update best score if neccessary
	ScoreHandler* score = Game::GetScoreHandler();
	if (score->GetLastScore() > score->GetBestScore()) {
		score->UpdateBestScore();
	}
	///
	std::string textureID = "main_atlas";
	int winWidth = 0, winHeight = 0;
	SDL_GetWindowSize(Game::GetWindow(), &winWidth, &winHeight);
	mainScene = new GameObject;
	///background
	int bkgHeight = 512;
	int bkgWidth  = 288;
	int bkgX = 0;
	int bkgY = 0;
	auto background = new GameObject;
	background->AddComponent(new StaticSprite(background, textureID,
		bkgX, bkgY, bkgWidth, bkgHeight));
	mainScene->AddChild(background);
	///ground
	int groundHeight = 112;
	int groundWidth  = 288;
	int groundX = 584;
	int groundY = 0;
	auto ground = new GameObject;
	ground->GetTransform()->Y = (float)winHeight - groundHeight;
	ground->AddComponent(new StaticSprite(ground, textureID, groundX, groundY,
		groundWidth, groundHeight));
	mainScene->AddChild(ground);
	///scoretable
	int scHeight = 114;
	int scWidth  = 226;
	int scX = 6;
	int scY = 518;
	auto scoretableObject = new GameObject;
	auto scoretable = new StaticSprite(scoretableObject, textureID,
		scX, scY, scWidth, scHeight);
	scoretableObject->AddComponent(scoretable);
	scoretableObject->GetTransform()->X = (winWidth - scWidth) * 0.5f;
	scoretableObject->GetTransform()->Y = (winHeight - scHeight) * 0.5f;
	mainScene->AddChild(scoretableObject);
	///score last value
	int slvX = 584;
	int slvY = 362;
	int slvW = 14;
	int slvH = 20;
	int slvMargin = 4;
	int slvMarginRight = -45;
	int slvMarginTop   = 35;
	auto scoreLastObject = new GameObject;
	auto slv = new ScoreDisplay(scoreLastObject, textureID, slvX, slvY, slvW, slvH,
		slvMargin, []()->int { return Game::GetScoreHandler()->GetLastScore(); });
	scoreLastObject->GetTransform()->X = scoretableObject->GetTransform()->X + scWidth + slvMarginRight;
	scoreLastObject->GetTransform()->Y = scoretableObject->GetTransform()->Y + slvMarginTop;
	scoreLastObject->AddComponent(slv);
	scoretableObject->AddChild(scoreLastObject);
	///score best value
	int sbvX = 584;
	int sbvY = 362;
	int sbvW = 14;
	int sbvH = 20;
	int sbvFrameMargin = 4;
	int sbvMarginRight = -45;
	int sbvMarginTop   = 75;
	auto scoreBestObject = new GameObject;
	auto sbv = new ScoreDisplay(scoreBestObject, textureID, sbvX, sbvY, sbvW, sbvH,
		sbvFrameMargin, []()->int { return Game::GetScoreHandler()->GetBestScore(); });
	
	scoreBestObject->GetTransform()->X = scoretableObject->GetTransform()->X + scWidth + sbvMarginRight;
	scoreBestObject->GetTransform()->Y = scoretableObject->GetTransform()->Y + sbvMarginTop;
	scoreBestObject->AddComponent(sbv);
	scoretableObject->AddChild(scoreBestObject);
	///achievents
	int medalW  = 44;
	int medalH  = 44;
	int bronzeX = 224;
	int bronzeY = 954;
	int silverX = 224;
	int silverY = 906;
	int goldX   = 242;
	int goldY   = 564;
	int medalOffsetX = 24;
	int medalOffsetY = 42;
	int lastScore = Game::GetScoreHandler()->GetLastScore();
	auto AddMedalLambda = [&](int x, int y, int w, int h) {
		auto medalObject = new GameObject;
		auto medal = new StaticSprite(medalObject, textureID, x, y, w, h);
		medalObject->GetTransform()->X = scoretableObject->GetTransform()->X + medalOffsetX;
		medalObject->GetTransform()->Y = scoretableObject->GetTransform()->Y + medalOffsetY;
		medalObject->AddComponent(medal);
		scoretableObject->AddChild(medalObject);
	};

	int bronzeScore = 10;
	int silverScore = 20;
	int goldScore   = 40;

	if (bronzeScore <= lastScore && lastScore < silverScore) {
		AddMedalLambda(bronzeX, bronzeY, medalW, medalH);
	} else if (silverScore <= lastScore && lastScore < goldScore) {
		AddMedalLambda(silverX, silverY, medalW, medalH);
	} else if (goldScore < lastScore) {
		AddMedalLambda(goldX, goldY, medalW, medalH);
	}
	///gameover sign
	int signX = 790;
	int signY = 118;
	int signWidth = 192;
	int signHeight = 42;
	auto signObject = new GameObject;
	signObject->GetTransform()->X = (winWidth - signWidth) * 0.5f;
	signObject->GetTransform()->Y = scoretableObject->GetTransform()->Y - (signHeight + 10);
	auto sign = new StaticSprite(signObject, textureID, signX, signY, signWidth, signHeight);
	signObject->AddComponent(sign);
	mainScene->AddChild(signObject);
	///button
	int btnX = 924;
	int btnY = 84;
	int btnW = 80;
	int btnH = 28;
	float btnXpos = scoretableObject->GetTransform()->X + scWidth / 2 - btnW / 2;
	float btnYpos = scoretableObject->GetTransform()->Y + scHeight + 10;
	auto btnHandler = []() { Game::GetStateHandler()->Replace(new PlayGameState); };
	auto btnObj = new GameObject(btnXpos, btnYpos);
	auto button = new Button(btnObj, textureID, btnX, btnY, btnW, btnH, btnHandler);
	btnObj->AddComponent(button);
	mainScene->AddChild(btnObj);
	///
	return true;
}