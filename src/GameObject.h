#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include <vector>

namespace FlappyBirdClone {
	class Component;
	class Transform;

	class GameObject {
	public:
		GameObject();
		GameObject(float x, float y);
		virtual ~GameObject();

		void Draw();
		void Update();
		void AddComponent(Component* component);
		void AddChild(GameObject* object);
		void RemoveChild(GameObject* object);
		template<class T> Component* GetComponent();
		Transform* GetTransform() const;

	private:
		std::vector<Component*> components;
		std::vector<GameObject*> objects;
	};

	template<class T>
	inline Component* GameObject::GetComponent()
	{
		for (auto& c : components) {
			if (dynamic_cast<T*>(c) == nullptr) {
				continue;
			}
			return c;
		}
		return nullptr;
	}
}
#endif // !_GAMEOBJECT_H_