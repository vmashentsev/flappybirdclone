#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_

#include <string>

namespace FlappyBirdClone {
	class GameObject;

	class GameState {
	public:
		virtual void Render() = 0;
		virtual void Update() = 0;
		virtual bool OnEnter() = 0;
		virtual bool OnExit() = 0;
		virtual int  GetStateID() = 0;
	};

	class MainMenuState : public GameState {
	public:
		virtual void Render();
		virtual void Update();
		virtual bool OnEnter();
		virtual bool OnExit();
		virtual int GetStateID() { return stateID; }
	private:
		GameObject* mainScene;

		static const int stateID = 0;
	};

	class PlayGameState : public GameState {
	public:
		virtual void Render();
		virtual void Update();
		virtual bool OnEnter();
		virtual bool OnExit();
		virtual int GetStateID() { return stateID; }
	
		enum class Substate { NORMAL, HITTED };

	private:

		GameObject* mainScene;
		Substate currentState = Substate::NORMAL;

		static const int stateID = 1;
	};

	class GameOverState: public GameState {
	public:
		virtual void Render();
		virtual void Update();
		virtual bool OnEnter();
		virtual bool OnExit();
		virtual int GetStateID() { return stateID; }

	private:
		GameObject* mainScene;

		static const int stateID = 2;
	};
}

#endif // !_GAMESTATE_H_