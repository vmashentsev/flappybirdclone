#ifndef _TEXTUREMANAGER_H_
#define _TEXTUREMANAGER_H_

#include <SDL_render.h>
#include <string>
#include <map>

namespace FlappyBirdClone {
	class TextureManager {
	public:
		~TextureManager();

		bool Load(const std::string& fileName, const std::string& id, SDL_Renderer* renderer);

		void Render(
			SDL_Renderer* renderer,
			const std::string& id,
			int sx, int sy, int w, int h,
			float dx, float dy);

		void RenderFrame(
			SDL_Renderer* renderer,
			const std::string& id,
			int sx, int sy, int w, int h, 
			float dx, float dy, int frame,
			int margin = 0, bool row = true);

		void RenderFrame(
			SDL_Renderer* renderer,
			const std::string& id,
			int sx, int sy, int w, int h,
			float dx, float dy, float angle, int frame,
			int margin = 0, bool row = true);

		void RenderFrameInterlaced(
			SDL_Renderer* renderer,
			const std::string& id,
			int sx, int sy, int w, int h,
			float dx, float dy, int offset,
			int frame, bool row = true);

	private:
		std::map<std::string, SDL_Texture*> textureMap;
	};
}
#endif // !_TEXTUREMANAGER_H_