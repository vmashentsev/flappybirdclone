#ifndef _SCOREHANDLER_H_
#define _SCOREHANDLER_H_

namespace FlappyBirdClone {
	class ScoreHandler {
	public:
		int GetBestScore() const { return bestScore; }
		int GetLastScore() const { return lastScore; }
		void ResetLastScore()  { lastScore = 0; }
		void IncLastScore()    { ++lastScore;   }
		void UpdateBestScore() { bestScore = lastScore; }

	private:
		int bestScore = 0;
		int lastScore = 0;
	};
}

#endif // !_SCOREHANDLER_H_