#include "SoundManager.h"
#include <iostream>

int FlappyBirdClone::SoundManager::Sfx::lastChannel = 0;

FlappyBirdClone::SoundManager::SoundManager()
{
	if (Mix_OpenAudio(freq, MIX_DEFAULT_FORMAT, 2, chunkSize) < 0) {
		isvalid = false;
		return;
	}
}

FlappyBirdClone::SoundManager::~SoundManager()
{
	for (auto& i : sounds) {
		Mix_FreeChunk(i.second.GetChunk());
	}
	Mix_Quit();
}

bool FlappyBirdClone::SoundManager::Load(const std::string& name, const std::string& filename)
{
	auto sfx = Mix_LoadWAV(filename.c_str());
	if (sfx == nullptr) {
		std::cerr << "Sound \"" << filename << "\" that is to load not found" << std::endl;
		return false;
	}
	sounds[name] = std::move(Sfx(sfx)); ///I think the move is not redundant
	return true;
}

void FlappyBirdClone::SoundManager::Play(const std::string& name)
{
	if (sounds.count(name) == 0) {
		std::cerr << "Sound \"" << name << "\" that is to play not found" << std::endl;
		return;
	}
	Mix_PlayChannel(sounds[name].GetChannel(), sounds[name].GetChunk(), 0);
}
