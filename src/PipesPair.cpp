#include "PipesPair.h"
#include "Game.h"
#include "Transform.h"

#include <iostream>

float FlappyBirdClone::PipesPair::lastRatio = 0.5f;
const float FlappyBirdClone::PipesPair::lowRatioBound = 0.15f;
const float FlappyBirdClone::PipesPair::highRatioBound = 0.6f;

std::default_random_engine FlappyBirdClone::PipesPair::generator =
	std::default_random_engine();

std::uniform_real_distribution<float> FlappyBirdClone::PipesPair::distr =
	std::uniform_real_distribution<float>(lowRatioBound, highRatioBound);

FlappyBirdClone::PipesPair::PipesPair(GameObject* parent, int totalHeight, float ratioDev)
	: Component(parent), textureID{ "main_atlas" }, totalHeight{ totalHeight },
	ratioDev{ ratioDev }
{
	float holePos = distr(generator);
	float minBound = lastRatio - ratioDev;
	float maxBound = lastRatio + ratioDev;
	///clamp
	if (holePos < minBound) {
		holePos = minBound;
	} else if (holePos > maxBound) {
		holePos = maxBound;
	}
	lastRatio = holePos;
	int pos = (int)(totalHeight * holePos);
	topPipeY = pos - holeHeight / 2;
	bottomPipeY = pos + holeHeight / 2;
}

void FlappyBirdClone::PipesPair::Draw()
{
	auto transform = GetParent()->GetTransform();
	///Top pipe
	Game::GetTextureManager()->Render(
		Game::GetRenderer(), textureID,
		xT, yT + height - topPipeY, width, topPipeY,
		transform->X, transform->Y);
	///Bottom pipe
	Game::GetTextureManager()->Render(
		Game::GetRenderer(), textureID,
		xB, yB, width, totalHeight - bottomPipeY,
		transform->X, transform->Y + bottomPipeY);
}