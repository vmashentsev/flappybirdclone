#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include "GameObject.h"

namespace FlappyBirdClone {
	class Component {
	public:
		virtual ~Component() { };
		virtual void Draw() {};
		virtual void Update() = 0;
		GameObject* GetParent() { return parent; }

	protected:
		Component(GameObject* parent): parent{ parent } {}

	private:
		GameObject* parent;
	};
}

#endif // !_COMPONENT_H_