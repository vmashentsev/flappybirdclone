#include "Game.h"
#include "InputHandler.h"
#include <SDL_events.h>

void FlappyBirdClone::InputHandler::Update()
{
	leftMouseDown = false;
	leftMouseUp = false;

	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_MOUSEBUTTONDOWN:
			OnMouseDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			OnMouseUp(event);
			break;
		case SDL_MOUSEMOTION:
			OnMouseMove(event);
			break;
		case SDL_QUIT:
			Game::Quit();
			break;
		default:
			break;
		}
	}
}

std::tuple<int, int> FlappyBirdClone::InputHandler::GetMousePosition() const
{
	return std::move(std::make_tuple(mousex, mousey));
}

void FlappyBirdClone::InputHandler::OnMouseButton(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT) {
		leftMousePress = !leftMousePress;
	}
}

void FlappyBirdClone::InputHandler::OnMouseMove(SDL_Event& event)
{
	mousex = event.motion.x;
	mousey = event.motion.y;
}

void FlappyBirdClone::InputHandler::OnMouseDown(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT) {
		if (!leftMousePress) {
			leftMouseDown = true;
		}
		leftMousePress = true;
	}
}

void FlappyBirdClone::InputHandler::OnMouseUp(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT) {
		leftMousePress = false;
		leftMouseUp = true;
		leftMouseDown = false;
	}
}