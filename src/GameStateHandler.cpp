#include "GameStateHandler.h"

void FlappyBirdClone::GameStateHandler::Add(GameState* state)
{
	gameStates.push(state);
	state->OnEnter();
}

void FlappyBirdClone::GameStateHandler::Remove()
{
	if (gameStates.empty()) {
		return;
	}
	if (!gameStates.top()->OnExit()) {
		return;
	}
	invalidatedStates.push(gameStates.top());
	isValid = false;
	gameStates.pop();
}

void FlappyBirdClone::GameStateHandler::Replace(GameState* state)
{
	if (gameStates.empty()) {
		Add(state);
		return;
	}
	///replacing one gamestate by another with the same type is meaningless
	if (gameStates.top()->GetStateID() == state->GetStateID()) {
		return;
	}
	if (gameStates.top()->OnExit()) {
		invalidatedStates.push(gameStates.top());
		isValid = false;
		gameStates.pop();
	}
	Add(state);
}

void FlappyBirdClone::GameStateHandler::Render()
{
	if (gameStates.empty() || !isValid) {
		return;
	}
	gameStates.top()->Render();
}

void FlappyBirdClone::GameStateHandler::Update()
{
	if (gameStates.empty()) {
		return;
	}
	while (!invalidatedStates.empty()) {
		delete invalidatedStates.front();
		invalidatedStates.pop();
	}
	isValid = true;
	gameStates.top()->Update();
}
