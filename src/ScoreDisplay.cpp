#include "ScoreDisplay.h"

#include "Game.h"
#include "Transform.h"
#include <stack>

void FlappyBirdClone::ScoreDisplay::Draw()
{
	std::stack<char> digits;
	while (score > 9) {
		digits.push(score % 10);
		score /= 10;
	}
	digits.push(score);
	///
	int signWidth = (width + margin) * digits.size() - margin;
	auto transform = GetParent()->GetTransform();
	float dx = transform->X - signWidth / 2;
	float dy = transform->Y;
	while (!digits.empty()) {
		int digit = digits.top();
		Game::GetTextureManager()->RenderFrame(Game::GetRenderer(), textureID,
			sx, sy, width, height, dx, dy, digit, margin);
		digits.pop();
		dx += (width + margin);
	}
}

void FlappyBirdClone::ScoreDisplay::Update()
{
	score = scoreGetter();
}