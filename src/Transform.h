#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "Component.h"

namespace FlappyBirdClone {
	class Transform : public Component {
	public:
		void Update() {}
		float X = 0;
		float Y = 0;
	private:
		Transform(GameObject* parent, float x, float y): Component(parent), X{ x }, Y{ y } {}
		friend class GameObject;
	};
}
#endif // !_TRANSFORM_H_