#ifndef _PIPESPAIR_H_
#define _PIPESPAIR_H_

#include "Component.h"
#include "Collidable.h"
#include <string>
#include <random>

namespace FlappyBirdClone {
	class PipesPair : public Component {
	public:
		PipesPair(GameObject* parent, int totalHeight, float ratioDev = 0.17f);
		virtual void Draw() override;
		virtual void Update() override {}

		int GetHeight() const { return totalHeight; }
		int GetWidth()  const { return width; }
		int GetTopPipeHeight()    const { return topPipeY; }
		int GetBottomPipeHeight() const { return totalHeight - bottomPipeY; }

	private:
		std::string textureID;
		const int holeHeight = 80;
		const int xT = 112;
		const int yT = 646;
		const int xB = 168;
		const int yB = 646;
		int width = 52;
		int height = 320;
		const int totalHeight;
		int topPipeY;
		int bottomPipeY;
		float ratioDev;

		static float lastRatio;
		static const float lowRatioBound;
		static const float highRatioBound;

		static std::default_random_engine generator;
		static std::uniform_real_distribution<float> distr;
	};
}
#endif // !_PIPESPAIR_H_