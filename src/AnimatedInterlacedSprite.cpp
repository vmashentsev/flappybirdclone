#include "AnimatedInterlacedSprite.h"
#include "Game.h"
#include "Transform.h"

void FlappyBirdClone::AnimatedInterlacedSprite::Draw()
{
	auto transform = GetParent()->GetTransform();
	Game::GetTextureManager()->RenderFrameInterlaced(
		Game::GetRenderer(), spriteID,
		sx, sy, width, height,
		transform->X, transform->Y, offset,
		frame, isRow);
}

void FlappyBirdClone::AnimatedInterlacedSprite::Update()
{
	frame = int(Game::GetClock()->GetTime() * framePerSec) % frameCount;
}