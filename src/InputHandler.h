#ifndef _INPUTHANDLER_H_
#define _INPUTHANDLER_H_

#include <SDL_events.h>
#include <tuple>

namespace FlappyBirdClone {
	class InputHandler {
	public:
		void Update();
		std::tuple<int, int> GetMousePosition() const;

		///Return true if LMB has been pressed and not upped (long press)
		bool GetLeftMousePress() const { return leftMousePress; }
		///Return true if LMB has been downed
		bool GetLeftMouseDown()  const { return leftMouseDown;  }
		///Return true if LMB has been upped
		bool GetLeftMouseUp()    const { return leftMouseUp;    }

	private:
		void OnMouseButton(SDL_Event& event);
		void OnMouseMove(SDL_Event& event);
		void OnMouseDown(SDL_Event& event);
		void OnMouseUp(SDL_Event& event);

		bool leftMousePress = false;
		bool leftMouseDown  = false;
		bool leftMouseUp    = false;
		int  mousex = 0;
		int  mousey = 0;
	};
}

#endif // !_INPUTHANDLER_H_