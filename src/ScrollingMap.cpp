#include "ScrollingMap.h"
#include "Game.h"
#include "Transform.h"
#include "PipesPair.h"
#include <SDL_timer.h>

FlappyBirdClone::ScrollingMap::ScrollingMap(GameObject* parent, AnimatedInterlacedSprite* ground,
	PlayGameState::Substate& substate, int width, int leftBorder)
	: Component(parent), ground{ ground }, playGameState{ substate },
	leftBorder{ leftBorder}, rightBorder{ leftBorder + width }
{
	int tempw = 0;
	SDL_GetWindowSize(Game::GetWindow(), &tempw, &heightRegion);
	AddPipe(350);
}

void FlappyBirdClone::ScrollingMap::Update()
{
	float dt = Game::GetClock()->GetDelta();
	for (auto& pipe : pipes) {
		pipe->GetParent()->GetTransform()->X += dt*scrollSpeed;
	}
	for (auto& trigger : triggers) {
		trigger->GetParent()->GetTransform()->X += dt*scrollSpeed;
	}
	auto lastPipe = pipes.back();
	auto transformLast = lastPipe->GetParent()->GetTransform();
	if (rightBorder - transformLast->X - lastPipe->GetWidth() > pipeInterval) {
		AddPipe(rightBorder);
	}
	auto firstPipe = pipes.front();
	auto firstPipeTransform = firstPipe->GetParent()->GetTransform();
	if (leftBorder > firstPipeTransform->X + firstPipe->GetWidth()) {
		RemoveFirstPipe();
	}
}

void FlappyBirdClone::ScrollingMap::AddPipe(int x)
{
	auto pipeObject = new GameObject;
	GetParent()->AddChild(pipeObject);
	pipeObject->GetTransform()->X = (float)x;
	auto pipe = new PipesPair(pipeObject, heightRegion);
	pipeObject->AddComponent(pipe);
	auto pipesCollider = new PipesPairCollider(0, 0,
		(float)(pipe->GetWidth()), (float)(pipe->GetHeight()),
		(float)(pipe->GetTopPipeHeight()), (float)(pipe->GetHeight() - pipe->GetBottomPipeHeight()));
	pipeObject->AddComponent(new HitCollision(pipeObject, pipesCollider, this, ground, playGameState));
	///score trigger
	auto scoreTriggerObject = new GameObject;
	GetParent()->AddChild(scoreTriggerObject);
	scoreTriggerObject->GetTransform()->X = (float)x + 5;
	scoreTriggerObject->GetTransform()->Y = (float)(pipe->GetTopPipeHeight());
	float triggerHeight = (float)pipe->GetHeight() - pipe->GetTopPipeHeight() - pipe->GetBottomPipeHeight();
	auto triggerCollider = new AABBCollider(0, 0, (float)pipe->GetWidth() - 5, triggerHeight);
	auto trigger = new TriggerCollision(scoreTriggerObject, triggerCollider);
	scoreTriggerObject->AddComponent(trigger);
	///
	pipes.push_back(pipe);
	triggers.push_back(trigger);
}

void FlappyBirdClone::ScrollingMap::RemoveFirstPipe()
{
	///Need to delete object. While deleting object, all components will be
	///deleted too and there is no need to delete PipesPair component
	GetParent()->RemoveChild(pipes.front()->GetParent());
	pipes.pop_front();
	GetParent()->RemoveChild(triggers.front()->GetParent());
	triggers.pop_front();
}