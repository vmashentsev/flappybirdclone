#include "Collidable.h"
#include "Game.h"
#include "GameObject.h"
#include "Component.h"
#include "ScrollingMap.h"
#include "Transform.h"

#include <iostream>

FlappyBirdClone::AABBCollider::AABBCollider(float x, float y, float width, float height)
{
	collider.x = (int)x;
	collider.y = (int)y;
	collider.w = (int)width;
	collider.h = (int)height;
}

//AABBCollider::AABBCollider(AABBCollider&& c)
//	: collider{ c.collider.x, c.collider.y, c.collider.w, c.collider.h } {
//
//}

FlappyBirdClone::AABBCollider::AABBCollider(const AABBCollider& c)
{
	collider.x = c.collider.x;
	collider.y = c.collider.y;
	collider.w = c.collider.w;
	collider.h = c.collider.h;
}

bool FlappyBirdClone::AABBCollider::Collide(const ICollider& other) const
{
	return other.Collide(*this);
}

bool FlappyBirdClone::AABBCollider::Collide(const AABBCollider& other) const
{
	return SDL_HasIntersection(&collider, &other.collider) == SDL_TRUE;
}

void FlappyBirdClone::AABBCollider::SetPosition(float x, float y)
{
	collider.x = (int)x;
	collider.y = (int)y;
}

FlappyBirdClone::PipesPairCollider::PipesPairCollider(float x, float y, float width, float height,
									 float topY, float botY)
{
	colliderTop.x = (int)x;
	colliderTop.y = (int)y;
	colliderTop.w = (int)width;
	colliderTop.h = (int)topY;
	
	colliderBot.x = (int)x;
	colliderBot.y = (int)botY;
	colliderBot.w = (int)width;
	colliderBot.h = (int)(height - botY);
}

void FlappyBirdClone::PipesPairCollider::SetPosition(float x, float y)
{
	colliderTop.x = (int)x;
	colliderTop.y = (int)y;
	colliderBot.x = (int)x;
	colliderBot.y += (int)y;
}

bool FlappyBirdClone::PipesPairCollider::Collide(const ICollider& other) const
{
	return other.Collide(*this);
}

bool FlappyBirdClone::PipesPairCollider::Collide(const AABBCollider& other) const
{
	if (SDL_HasIntersection(&colliderTop, &other.GetCollider()) == SDL_TRUE) {
		return true;
	}
	if (SDL_HasIntersection(&colliderBot, &other.GetCollider()) == SDL_TRUE) {
			return true;
	}
	return false;
}

FlappyBirdClone::Collidable::Collidable(GameObject* parent, ICollider* collider)
	: Component(parent), collider{ collider }
{
	Game::GetCollisionManager()->AddCollidable(this); // warning for this
}

FlappyBirdClone::Collidable::~Collidable()
{
	Game::GetCollisionManager()->RemoveCollidable(this);
	delete collider;
}

const FlappyBirdClone::ICollider* FlappyBirdClone::Collidable::GetCollider()
{
	auto transform = GetParent()->GetTransform();
	collider->SetPosition(transform->X, transform->Y);
	return collider;
}

FlappyBirdClone::CollisionStatus FlappyBirdClone::Collidable::Collide(Collidable* other)
{
	bool collide = GetCollider()->Collide(*(other->GetCollider()));
	auto col = std::find(activeCol.begin(), activeCol.end(), other);
	auto exist = activeCol.end() != col;
	bool enterCollide = collide & !exist;
	bool exitCollide = !collide & exist;
	if (enterCollide) {
		activeCol.insert(other);
	} else if (exitCollide) {
		activeCol.erase(col);
	}
	return std::move(CollisionStatus(collide, enterCollide, exitCollide));
}

bool FlappyBirdClone::EndGameCollision::OnCollision(Collidable* other)
{
	std::cout << "collision detected" << std::endl;
	Game::GetSoundManager()->Play("die");
	Game::GetStateHandler()->Replace(new GameOverState);
	return true;
}

bool FlappyBirdClone::HitCollision::OnCollisionEnter(Collidable* other)
{
	Game::GetSoundManager()->Play("hit");
	map->StopScrolling();
	ground->StopAnimation();
	playGameState = PlayGameState::Substate::HITTED;
	return false;
}

bool FlappyBirdClone::TriggerCollision::OnCollisionEnter(Collidable* other)
{
	Game::GetSoundManager()->Play("point");
	Game::GetScoreHandler()->IncLastScore();
	return false;
}