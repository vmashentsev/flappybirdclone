#include "Game.h"
#include <iostream>
#include <string>

FlappyBirdClone::Game FlappyBirdClone::Game::instance = Game();

void FlappyBirdClone::Game::Clean()
{
	delete soundManager;
	delete gameClock;
	delete scoreHandler;
	delete gameStateHandler;
	delete collisionManager;
	delete textureManager;
	delete inputHandler;
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

inline bool FlappyBirdClone::Game::Init()
{
	//instance = Game();
	if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO)) {
		std::string errorString(SDL_GetError());
		std::cout << errorString << std::endl;
		return false;
	}
	window = SDL_CreateWindow("Test", 50, 50, 288, 512, 0);
	if (nullptr == window) {
		std::string errorString(SDL_GetError());
		std::cout << errorString << std::endl;
		return false;
	}
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (nullptr == renderer) {
		std::string errorString(SDL_GetError());
		std::cout << errorString << std::endl;
		return false;
	}
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	inputHandler     = new InputHandler;
	textureManager   = new TextureManager;
	collisionManager = new CollisionManager;
	gameStateHandler = new GameStateHandler;
	scoreHandler     = new ScoreHandler;
	gameClock        = new ClockChrono;
	gameStateHandler->Add(new MainMenuState);
	///init sound system
	soundManager = new SoundManager;
	if (!soundManager->IsValid()) {
		std::cerr << "Sound subsystem couldn't inited" << std::endl;
		return false;
	}
	bool soundValid = soundManager->Load("hit", "Assets\\sfx_hit.wav");
	soundValid &= soundManager->Load("point", "Assets\\sfx_point.wav");
	soundValid &= soundManager->Load("wing", "Assets\\sfx_wing.wav");
	soundValid &= soundManager->Load("die", "Assets\\sfx_die.wav");
	if (!soundValid) {
		std::cerr << "Some sound assets couldn't loaded" << std::endl;
		return false;
	}
	return true;
}

void FlappyBirdClone::Game::HandleEvents()
{
	inputHandler->Update();
}

void FlappyBirdClone::Game::Render()
{
	SDL_RenderClear(renderer);
	gameStateHandler->Render();
	SDL_RenderPresent(renderer);
}

void FlappyBirdClone::Game::Run()
{
	if (!instance.Init()) {
		return;
	}
	instance.isRunning = true;
	while (instance.isRunning) {
		instance.HandleEvents();
		instance.gameClock->Stop();
		instance.Update();
		instance.gameClock->Start();
		instance.Render();
	}
	instance.Clean();
}

void FlappyBirdClone::Game::Update()
{
	gameStateHandler->Update();
}
