#ifndef _COLLIDABLE_H_
#define _COLLIDABLE_H_

#include "Component.h"
#include "CollisionManager.h"
#include "GameState.h"
#include "AnimatedInterlacedSprite.h"
#include <SDL_rect.h>
#include <unordered_set>

namespace FlappyBirdClone {
	class ScrollingMap;

	class AABBCollider;

	struct CollisionStatus {
		CollisionStatus(bool col, bool colEnter, bool colExit):
			Collision{ col }, CollisionEnter{ colEnter }, CollisionExit{ colExit } {}
	
		bool Collision      = false;
		bool CollisionEnter = false;
		bool CollisionExit  = false;
	};

	class ICollider {
	public:
		virtual bool Collide(const ICollider&) const = 0;
		virtual bool Collide(const AABBCollider&)  const = 0;
		virtual void SetPosition(float x, float y) = 0;
	};

	class AABBCollider: public ICollider {
	public:
		//todo delete x y parameters
		AABBCollider(float x, float y, float width, float height);
		AABBCollider(const AABBCollider&);

		virtual bool Collide(const ICollider&) const override;
		virtual bool Collide(const AABBCollider&)  const override;

		virtual void SetPosition(float x, float y) override;
		const SDL_Rect& GetCollider() const { return collider; }

	private:
		SDL_Rect collider;
	};

	class PipesPairCollider: public ICollider {
	public:
		PipesPairCollider(float x, float y, float width, float height, float topY, float botY);
		virtual bool Collide(const ICollider&) const override;
		virtual bool Collide(const AABBCollider&) const override;
		virtual void SetPosition(float x, float y) override;
	private:
		SDL_Rect colliderTop;
		SDL_Rect colliderBot;
	};

	class Collidable: public Component {
	public:
		Collidable(GameObject* parent, ICollider* collider);
		virtual ~Collidable();

		virtual void Update() override {};
		virtual bool OnCollision(Collidable*) { return false; }
		virtual bool OnCollisionEnter(Collidable*) { return false; }
		CollisionStatus Collide(Collidable* other);

	private:
		const ICollider* GetCollider();

		std::unordered_set<Collidable*> activeCol;

		ICollider* collider;
	};

	class EmptyCollision: public Collidable {
	public:
		EmptyCollision(GameObject* parent, ICollider* collider)
			: Collidable(parent, collider) {}
	};

	class EndGameCollision: public Collidable {
	public:
		EndGameCollision(GameObject* parent, ICollider* collider)
			: Collidable(parent, collider) {}

		virtual bool OnCollision(Collidable*) override;
	};

	class HitCollision : public Collidable {
	public:
		HitCollision(GameObject* parent, ICollider* collider, ScrollingMap* map,
			AnimatedInterlacedSprite* ground, PlayGameState::Substate& substate)
			: Collidable(parent, collider), map(map), ground{ ground },
			playGameState{ substate	} {}
		virtual bool OnCollisionEnter(Collidable*) override;

	private:
		ScrollingMap* map;
		AnimatedInterlacedSprite* ground;
		PlayGameState::Substate& playGameState;
	};

	class TriggerCollision: public Collidable {
	public:
		TriggerCollision(GameObject* parent, ICollider* collider)
			: Collidable(parent, collider) {}

		virtual bool OnCollisionEnter(Collidable*) override;
	};
}
#endif // !_COLLIDABLE_H_