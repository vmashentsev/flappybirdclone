#ifndef _ANIMATEDSPRITE_H_
#define _ANIMATEDSPRITE_H_

#include "Component.h"

namespace FlappyBirdClone {
	class AnimatedInterlacedSprite: public Component {
	public:
		AnimatedInterlacedSprite(
			GameObject* parent, const std::string& id,
			int x, int y, int w, int h, int margin = 1,
			int frame = 0, bool isRow = true)
			: Component(parent), spriteID{ id },
			sx{ x }, sy{ y }, width{ w }, height{ h }, offset{ margin },
			frame{ frame }, isRow{ isRow }	{}

		void StopAnimation() { framePerSec = 0; }

		virtual void Draw() override;
		virtual void Update() override;

	private:
		std::string spriteID;
		int framePerSec = 40;
		int frameCount  = 12;
		int frame;
		int width;
		int height;
		int sx;
		int sy;
		int offset;
		bool isRow;
	};
}

#endif // !_ANIMATEDSPRITE_H_