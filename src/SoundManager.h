#ifndef _SOUNDMANAGER_H_
#define _SOUNDMANAGER_H_

#include <SDL_mixer.h>
#include <string>
#include <map>

namespace FlappyBirdClone {
	class SoundManager {
	public:
		SoundManager();
		~SoundManager();

		bool Load(const std::string& name, const std::string& filename);
		void Play(const std::string& name);
		bool IsValid() const { return isvalid; }

	private:
		class Sfx {
		public:
			Sfx(): chunk{ nullptr }, channel{ -1 } { }
			Sfx(Mix_Chunk* chunk): chunk{ chunk }, channel{GetNewChannel()} { }

			Mix_Chunk* GetChunk() const { return chunk;   }
			int GetChannel()      const { return channel; }

			static int GetNewChannel() { return lastChannel++; }
		private:
			Mix_Chunk* chunk;
			int channel;

			static int lastChannel;
		};

		int freq      = 44100;
		int chunkSize = 4096;
		bool isvalid  = true;
		std::map<std::string, Sfx> sounds;
	};
}
#endif // !_SOUNDMANAGER_H_