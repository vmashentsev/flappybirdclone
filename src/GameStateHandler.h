#ifndef _GAMESTATEHANDLER_H_
#define _GAMESTATEHANDLER_H_

#include "GameState.h"
#include <stack>
#include <queue>
#include <list>

namespace FlappyBirdClone {
	class GameStateHandler {
	public:
		void Add(GameState* state);
		void Remove();
		void Replace(GameState* state);

		void Render();
		void Update();

		bool IsValid() const { return isValid; }
		bool Invalidate() { isValid = false; }
	private:
		std::stack<GameState*> gameStates;
		std::queue<GameState*, std::list<GameState*>> invalidatedStates;
		bool isValid = true;
	};
}

#endif // !_GAMESTATEHANDLER_H_