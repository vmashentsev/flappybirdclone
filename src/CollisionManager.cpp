#include "CollisionManager.h"
#include "Collidable.h"

bool FlappyBirdClone::CollisionManager::CheckCollision(Collidable* active)
{
	for (auto& i : objects) {
		if (i == active) {
			continue;
		}
		if (Collide(active, i)) {
			return true;
		}
	}
	return false;
}

void FlappyBirdClone::CollisionManager::AddCollidable(Collidable* object)
{
	objects.push_back(object);
}

void FlappyBirdClone::CollisionManager::RemoveCollidable(Collidable* object)
{
	auto elem = std::find(objects.begin(), objects.end(), object);
	if (elem == objects.end()) {
		return;
	}
	objects.erase(elem);
}

bool FlappyBirdClone::CollisionManager::Collide(Collidable* active, Collidable* other)
{
	auto colStatus = active->Collide(other);
	if (colStatus.CollisionEnter) {
		if (other->OnCollisionEnter(active)) {
			return true;
		}
	}
	if (colStatus.Collision) {
		if (other->OnCollision(active)) {
			return true;
		}
	}
	return false;
}