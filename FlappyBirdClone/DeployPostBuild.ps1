param (
	[parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][string]$BuildDir    = "..\build",
	[parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][string]$Platform    = "x86",
	[parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][string]$Config      = "Debug",
	[parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][string]$SolutionDir = "",
	[parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][string]$ExecPath    = "",
	[parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][string]$AssetsDir   = ""
)
New-Item $BuildDir\$Platform\$Config\ -type directory -force
Copy-Item $SolutionDir\3rdparty\SDL2-2.0.5\lib\$Platform\*.dll $BuildDir\$Platform\$Config\ -force
Copy-Item $SolutionDir\3rdparty\SDL2_image-2.0.1\lib\$Platform\*.dll $BuildDir\$Platform\$Config\ -force
Copy-Item $SolutionDir\3rdparty\SDL2_mixer-2.0.1\lib\$Platform\*.dll $BuildDir\$Platform\$Config\ -force
Copy-Item $ExecPath $BuildDir\$Platform\$Config\ -force
Copy-Item $AssetsDir $BuildDir\$Platform\$Config\ -recurse -force
